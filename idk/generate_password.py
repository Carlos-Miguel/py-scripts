import random
import string
import sys

def generate_password(length=12):
    lowercase_letters = string.ascii_lowercase
    uppercase_letters = string.ascii_uppercase
    digits = string.digits
    special_characters = '!@#$%^&*()_+[]{}|;:,.<>?'

    all_characters = lowercase_letters + uppercase_letters + digits + special_characters

    if length < 8:
        raise ValueError("Password length should be at least 8 characters")

    password = ''.join(random.choice(all_characters) for _ in range(length))
    
    return password

if __name__ == "__main__":
    password_length = 8
    if len(sys.argv) == 2:
        if(sys.argv[1].isdigit()):
            password_length = int(sys.argv[1])        
        else:
            print("Usage: python generate_password.py <password_length>")
            sys.exit(1)

    print(generate_password(password_length))