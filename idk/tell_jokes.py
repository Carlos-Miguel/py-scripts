import random
import time

def tell_random_joke():
    jokes = [
        ("Why do programmers always mix up Christmas and Halloween?", 
         "Because Oct 31 == Dec 25!"),
        ("Why don't programmers like nature?", 
         "It has too many bugs."),
        ("How do you comfort a JavaScript bug?", 
         "You console it!"),
        ("Why did the developer go broke?", 
         "Because he used up all his cache!"),
        ("Why did the programmer quit his job?", 
         "He didn't get arrays!"),
        ("What's a programmer's favorite place in New York?", 
         "Manhattan."),
        ("Why did the SQL query go to therapy?", 
         "It had too many relational issues.")
    ]

    setup, punchline = random.choice(jokes)
    return setup, punchline

if __name__ == "__main__":

    setup, punchline = tell_random_joke()
    print(setup)
    time.sleep(2)  # Add a 2-second delay
    print(punchline)