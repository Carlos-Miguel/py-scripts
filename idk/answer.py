import random

def random_response():
    responses = ["Yes", "No"]
    weights = [1, 1]  
    return random.choices(responses, weights=weights)[0]

if __name__ == "__main__":
    while True:
        question = input("Ask a question (or type 'exit' to quit): ")
        
        if question.lower() == "exit":
            break
        
        answer = random_response()
        print(f"Answer: {answer}")