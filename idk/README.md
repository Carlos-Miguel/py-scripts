# I don't know ???

* answerByYes => will mostly answer "Yes" to your questions

`python answerByYes.py`

* answerByNo => will mostly answer "No" to your questions

`python answerByNo.py`

* answer => 50% "Yes" / 50% "No"

`python answer.py`

* Tell jokes

`python tell_jokes.py`

* Generate random password

`python generate_password.py <OPT: password_length>`

* Roll dice :  script for rolling a six-sided dice

`python roll_dice.py`
