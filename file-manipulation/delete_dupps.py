import os
import hashlib
import sys


def find_duplicates(directory):
    hash_dict = {}

    for foldername, subfolders, filenames in os.walk(directory):
        for filename in filenames:
            file_path = os.path.join(foldername, filename)
            
            with open(file_path, "rb") as file:
                file_hash = hashlib.md5(file.read()).hexdigest()
            
            if file_hash in hash_dict:
                hash_dict[file_hash].append(file_path)
            else:
                hash_dict[file_hash] = [file_path]

    return {k: v for k, v in hash_dict.items() if len(v) > 1}

def delete_duplicates(duplicate_dict):
    for hash_value, files in duplicate_dict.items():
        print(f"Deleting duplicates for hash: {hash_value}")
        for file in files[1:]:
            try:
                os.remove(file)
                print(f"Deleted: {file}")
            except Exception as e:
                print(f"Error deleting {file}: {str(e)}")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python delete_dupps.py <source_directory>")
        sys.exit(1)

    directory = sys.argv[1]

    if not os.path.exists(directory):
        print(f"The directory '{directory}' does not exist.")
    else:
        duplicate_files = find_duplicates(directory)
        if duplicate_files:
            delete_duplicates(duplicate_files)
            print("Duplicates deleted successfully.")
        else:
            print("No duplicate files found in the directory.")
