import os
import sys

def rename_files_with_string(directory, old_string, new_string):
    if not os.path.exists(directory):
        print(f"The directory '{directory}' does not exist.")
        return
    
    files = os.listdir(directory)

    for filename in files:
        if old_string in filename:
            new_filename = filename.replace(old_string, new_string)
            old_filepath = os.path.join(directory, filename)
            new_filepath = os.path.join(directory, new_filename)
            
            try:
                os.rename(old_filepath, new_filepath)
                print(f"Renamed '{filename}' to '{new_filename}'")
            except Exception as e:
                print(f"Error renaming '{filename}': {str(e)}")

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("Usage: python rename_files.py <directory> <old_string> <new_string>")
        sys.exit(1)

    directory = sys.argv[1]
    old_string = sys.argv[2]
    new_string = sys.argv[3]

    rename_files_with_string(directory, old_string, new_string)
