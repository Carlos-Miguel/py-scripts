import os
import sys
import shutil

def copy_files(source, destination):
    if not os.path.exists(source):
        print(f"The source directory '{source}' does not exist.")
        return
    
    if not os.path.exists(destination):
        os.makedirs(destination)

    files_to_copy = os.listdir(source)

    for file in files_to_copy:
        source_path = os.path.join(source, file)
        destination_path = os.path.join(destination, file)

        if os.path.isfile(source_path):
            try:
                shutil.copy2(source_path, destination_path)  # Use shutil.copy2 to preserve metadata
                print(f"File '{file}' copied successfully to '{destination}'.")
            except Exception as e:
                print(f"Error copying file '{file}': {str(e)}")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python copy_files.py <source_directory> <destination_directory>")
        sys.exit(1)

    source = sys.argv[1]
    destination = sys.argv[2]

    copy_files(source, destination)