import docx2txt
import pandas as pd
import sys

def word_to_dataframe(word_file):
    text = docx2txt.process(word_file)
    
    # Split the text into paragraphs if you wanna do something special you need to change this
    paragraphs = text.split('\n')
    
    # Create a DataFrame from the paragraphs
    df = pd.DataFrame({'Text': paragraphs})
    
    return df

def save_dataframe_to_csv(dataframe, output_csv):
    dataframe.to_csv(output_csv, index=False)
    print(f'DataFrame saved to {output_csv}')

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python word_to_csv.py <source_word> <destination_file>")
        sys.exit(1)

    word_file = sys.argv[1]
    output_csv = sys.argv[2]

    df = word_to_dataframe(word_file)
    save_dataframe_to_csv(df, output_csv)