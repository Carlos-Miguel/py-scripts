import os
import sys
import shutil

def move_files(source, destination):
    if not os.path.exists(source):
        print(f"The source directory '{source}' does not exist.")
        return
    
    if not os.path.exists(destination):
        os.makedirs(destination)

    files_to_move = os.listdir(source)

    for file in files_to_move:
        source_path = os.path.join(source, file)
        destination_path = os.path.join(destination, file)

        if os.path.isfile(source_path):
            try:
                shutil.move(source_path, destination_path)
                print(f"File '{file}' moved successfully to '{destination}'.")
            except Exception as e:
                print(f"Error moving file '{file}': {str(e)}")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python move_files.py <source_directory> <destination_directory>")
        sys.exit(1)

    source = sys.argv[1]
    destination = sys.argv[2]

    move_files(source, destination)