# Simple console games

* Guess the number

`python guessNumber.py`

![Guess the number screenshot](images/guessNumber.png)

* Rock / Paper / Scissors

`python rockPaperScissors.py`

![Rock / Paper / Scissors screenshot](images/rockPaperScissors.png)

* Tic-Tac-Toe

`python ticTacToe.py`

![Tic-Tac-Toe screenshot](images/ticTacToe.png)