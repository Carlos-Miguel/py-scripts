import random

def play_guess():
    min_number = 1
    max_number = 100
    secret_number = random.randint(min_number, max_number)

    max_attempts = 10
    attempts = 0

    print(f"Welcome to the Guess the Number game! You have {max_attempts} attempts to guess the number between {min_number} and {max_number}.")

    while attempts < max_attempts:
        try:
            # Get the player's guess
            guess = int(input("Enter your guess: "))
            
            # Increment the number of attempts
            attempts += 1
            
            # Check if the guess is correct
            if guess == secret_number:
                print(f"Congratulations! You guessed the number {secret_number} correctly in {attempts} attempts.")
                break
            elif guess < secret_number:
                print("Too low! Try a higher number.")
            else:
                print("Too high! Try a lower number.")
        except ValueError:
            print("Invalid input. Please enter a valid number.")

    if attempts == max_attempts:
        print(f"Sorry, you've run out of attempts. The secret number was {secret_number}. Better luck next time!")

if __name__ == "__main__":
    play_guess()